import {Injectable} from '@angular/core';
import jsonContent from './app.component.json';
import {
  convertComponentConfigToFroalaElements,
  convertFroalaElementsToComponentConfig,
  generateEditorsFromFroalaNames,
  getElementType,
  mapImagesToFiles
} from './shared/helpers';
import {HttpClient} from '@angular/common/http';
import {GIT_URL} from './shared/urls';
import {forkJoin, Observable, Observer} from 'rxjs';

@Injectable()
export class AppComponentService {
  constructor(private httpClient: HttpClient) {
  }

  public getInitialElementsState(): any {
    return convertComponentConfigToFroalaElements(jsonContent);
  }

  public getInitialEditorsState(): any {
    return generateEditorsFromFroalaNames(jsonContent);
  }

  public sendChanges(froalaElements: any): void {
    const $links = Object.entries(froalaElements).reduce((acc, [key, froalaElement]): any => getElementType(jsonContent, key) === 'image'
      // @ts-ignore
      ? [...acc, this.getBase64ImageFromBlobURL(key, froalaElement.src)]
      : [...acc], []);

    forkJoin($links).subscribe((imagesInBase64: any) => {
      const mappedBase64Files = mapImagesToFiles(jsonContent, imagesInBase64);

      this.sendFroalaElements(froalaElements, mappedBase64Files).subscribe((data: any) => {
        console.log(data);
      }, (err) => {
        console.error(err);
      });
    });
  }

  public sendFroalaElements(froalaElements: any, files: any): Observable<any> {
    const options = {headers: {'Content-Type': 'application/json'}};
    const froalaData = convertFroalaElementsToComponentConfig(jsonContent, froalaElements);
    const sendData = { ...froalaData, files };

    return this.httpClient.post(GIT_URL, JSON.stringify(sendData), options);
  }

  public getBase64ImageFromBlobURL(name: string, url: string) {
    return new Observable((observer: Observer<any>) => {
      this.httpClient.get(url, {
        responseType: 'blob'
      }).subscribe((blob: any) => {
        const reader = new FileReader();
        reader.readAsDataURL(blob);

        reader.onloadend = function() {
          const base64data = reader.result;
          observer.next({name, base64: base64data});
          observer.complete();
        };

        reader.onerror = (err) => {
          observer.error(err);
        };
      });
    });
  }
}
